import React from 'react';
  import { StyleSheet, Text, View } from 'react-native';
  import { createAppContainer, createSwitchNavigator } from 'react-navigation'
  import { createStackNavigator } from 'react-navigation-stack'
  import ListHome from './app/views/ListHome';
  import { setNavigator } from './app/components/OurFlatList/natigationRef';
  import Details from './app/views/Details';
  import Login from './app/views/Login';
  import Icon from 'react-native-vector-icons/Ionicons'
import LoginScreen from './app/views/Login';
  
  const mapListFlow = createStackNavigator({
    ListHome: ListHome,
    Details: Details,
  })
  
  mapListFlow.navigationOptions = {
    tabBarIcon: ({ tintColor }) => (
      <Icon name="ios-list" color={ tintColor } size={ 30 } />
    )
  }
  
  const switchNavigator = createSwitchNavigator({
      Login: {
      screen: LoginScreen
      },
      mapListFlow
  })
  
  const App = createAppContainer(switchNavigator)
  
  export default () => {
    return (
      <App 
        ref={navigator => {
          setNavigator(navigator)
        }}
      />
    );
  }
