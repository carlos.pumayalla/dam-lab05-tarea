import React, {Component,useState} from 'react';
import { Container, Button, Header, Item, Input, Footer, Content, Form, TextInput, View } from 'react-native';


function LoginScreen({navigation}){
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  return(
      <View>
            <TextInput type="Text" placeholder="Username" value={email} onChangeText={(email)=> setEmail(email)} />
            <TextInput type="Password" placeholder="Password"  keyboardType={null}value={password}
             onChangeText={(password)=> setPassword(password)} />
            <Button 
            title= "inciar sesión"
            onPress={()=> {
              if (email == "carlos@123" && password == "carlos"){
                navigation.navigate('ListHome')
                setEmail('')
                setPassword('')
              }else{
                console.log("CREDENCIALES ERRONEAS")
              }
            }
            }>
            </Button>
        </View>
  )
}

export default LoginScreen


