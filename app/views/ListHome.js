import React from 'react'
  import { SafeAreaView } from 'react-navigation'
  import { Text, StyleSheet, FlatList, TouchableOpacity, View, Button } from 'react-native'
  import { ListItem } from 'react-native-elements'
import { color } from 'react-native-elements/dist/helpers'
  
  const response = [
    {
        id: 1,
        name: 'Ceviche Peruano',
        precio: 'S/.29',
        descripcion: "Ceviche de Caballa recien pescada",
        image: require('./../img/ceviche.png')
    },
    {
        id: 2,
        name: 'Costillas de Cerdo',
        precio: 'S/.25',
        descripcion: "Costillas de Cerdo hechas a leña",
        image: require('./../img/costillas.png')
    },
    {
        id: 3,
        name: 'Filete de pollo',
        precio: 'S/.29',
        descripcion: "Filete de pollo tierno",
        image: require('./../img/filete.png')
    }
  ]
  
  const ListHome = ({ navigation }) => {
    return (
      <>
        <SafeAreaView style={ styles.container } forceInset={{ top: 'always' }}>
          <FlatList 
            data={ response }
            keyExtractor={ item => item.id }
            renderItem={({ item }) => {
              return (
                <TouchableOpacity
                  onPress={() => 
                    navigation.navigate('Details', {
                        _id: item.id,
                        title: item.name,
                        descrip: item.descripcion,
                        img: item.image})
                }>
                <View>
                    <Text style={styles.text2}> {item.name}</Text>
                    <Text style={styles.text}> {item.precio}</Text>
                </View>
                </TouchableOpacity>
              )
            }}
          />
          <View>
            <Button 
            title= "cerrar sesión"
            onPress={()=> navigation.navigate('Login')
            }/>
          </View>
        </SafeAreaView>
      </>
    )
  }
  
  ListHome.navigationOptions = {
    tabBarIcon: ({ tintColor }) => (
      <Icon name="ios-list" color={ tintColor } size={ 30 } />
    )
  }
  
  const styles = StyleSheet.create({
    container: {
      margin: 4,
      padding: 30
    },
    text2: {
        alignItems: 'center',
        backgroundColor: '#A4A4A4',
        color: 'black',
        fontWeight: 'bold',
        padding: 10,
        fontSize: 20,
      },
    text: {
        alignItems: 'center',
        backgroundColor: '#A4A4A4',
        color: 'white',
        fontWeight: 'bold',
        padding: 10,
        fontSize: 20,
    }
  })
  
  export default ListHome